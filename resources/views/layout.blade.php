<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/style.css">
        <title>Test</title>
    </head>
    <body>
        <div class="container">
            <ul id="navi">
                <li><a href="{{ route('weather') }}">Текущая температура в Брянске</a></li>
                <li><a href="{{ route('orders') }}">Список заказов</a></li>
            </ul>
            <hr>
            @yield('content')
        </div>
    </body>
</html>
