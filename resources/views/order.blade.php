@extends('layout')

@section('content')
    <h1>Заказ #{{ $order['id'] }}</h1>
    <form method="POST" action="{{ route('order-update', ['id' => $order['id']]) }}">
        <div class="form-group{{ $errors->has('client_email') ? ' has-error' : '' }}">
            <label for="client_email">email_клиента</label>
            <input type="text" id="client_email" name="client_email" class="form-control" value="{{ $order['client_email'] }}">
            @if ($errors->has('client_email'))
                <span id="helpBlock" class="help-block">{{ $errors->first('client_email') }}</span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('partner_name') ? ' has-error' : '' }}">
            <label for="partner_name">партнер</label>
            <input type="text" id="partner_name" name="partner_name" class="form-control" value="{{ $order['partner']['name'] }}">
            @if ($errors->has('partner_name'))
                <span id="helpBlock" class="help-block">{{ $errors->first('partner_name') }}</span>
            @endif
        </div>
        <div class="form-group">
            <strong>продукты</strong>
            <ul>
                @foreach($order['order_products'] as $product)
                    <li>{{ $product['product']['name'] }}, {{ $product['quantity'] }} шт.</li>
                @endforeach
            </ul>
        </div>
        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
            <label for="status">статус_заказа</label>
            <select id="status" name="status" class="form-control">
                @foreach($statuses as $id => $name)
                    <option value="{{ $id }}"{{ $id == $order['status']['id'] ? ' selected' : '' }}>{{ $name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <strong>стоимость_заказ</strong>
            <p class="lead">{{ number_format($order['price'], 0, '', ' ') }}</p>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-default">Сохранить</button>
    </form>
@stop
