@extends('layout')

@section('content')
    <h1>Заказы</h1>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <th>ид_заказа</th>
                <th>название_партнера</th>
                <th>стоимость_заказа</th>
                <th>наименование_состав_заказа</th>
                <th>статус_заказа</th>
            </thead>
            <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td><a href="{{ route('order', ['id' => $order['id']]) }}" target="_blank">{{ $order['id'] }}</a></td>
                        <td>{{ $order['partner_name'] }}</td>
                        <td>{{ number_format($order['price'], 0, '', ' ') }}</td>
                        <td>{{ $order['products'] }}</td>
                        <td>{{ $order['status_name'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop
