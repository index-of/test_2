<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    private static $statuses = [
         0 => 'новый',
        10 => 'подтвержден',
        20 => 'завершен',
    ];


    public function getStatusAttribute($val)
    {
        return [
            'id'   => $val,
            'name' => self::$statuses[$val],
        ];
    }

    public function partner()
    {
        return $this->belongsTo('App\Partner');
    }
    public function order_products()
    {
        return $this->hasMany('App\OrderProduct');
    }

    public static function statuses()
    {
        return self::$statuses;
    }
}
