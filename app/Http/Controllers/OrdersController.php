<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Order;

class OrdersController extends Controller
{
    public static function getIndex()
    {
        $orders = Order
            ::with(['partner', 'order_products.product'])
            ->get()
            ->toArray();

        foreach ($orders as &$order) {

            $price    = 0;
            $products = [];

            foreach ($order['order_products'] as $product) {
                $price     += $product['quantity'] * $product['price'];
                $products[] = $product['product']['name'];
            }

            $order = [
                'id'           => $order['id'],
                'partner_name' => $order['partner']['name'],
                'price'        => $price,
                'products'     => join(', ', $products),
                'status_name'  => $order['status']['name'],
            ];
        }

        return view('orders', ['orders' => $orders]);
    }

    public static function getOrder($id)
    {
        $order = Order
            ::with(['partner', 'order_products.product'])
            ->find($id);

        if (! $order) {
            abort(404);
        }

        $order = $order->toArray();
        $price = 0;

        foreach ($order['order_products'] as $product) {
            $price += $product['quantity'] * $product['price'];
        }

        $order['price'] = $price;

        return view(
            'order',
            ['order' => $order, 'statuses' => Order::statuses()]
        );
    }

    public static function updateOrder(Request $request, $id)
    {
        $order = Order::with(['partner'])->find($id);

        if (! $order) {
            abort(404);
        }

        $data = $request->validate(
            [
                'client_email' => 'required|email|max:255',
                'partner_name' => 'required|max:255',
                'status'       => 'in:' . join(
                    ',', array_keys(Order::statuses())
                ),
            ],
            [
                'required'     => 'Обязательное поле',
                'max'          => 'Более :max символов',
                'email'        => 'Неверный Email адрес',
            ]
        );

        $order->client_email = $data['client_email'];
        $order->status = $data['status'];
        $order->save();

        $order->partner->name = $data['partner_name'];
        $order->partner->save();

        return redirect()->route('order', ['id' => $id]);
    }
}
