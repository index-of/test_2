<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;

class WeatherController extends Controller
{
    public static function getIndex()
    {
        // тестовый тариф
        $result = @file_get_contents(
            sprintf(
                'https://api.weather.yandex.ru/v1/forecast?lat=%s&lon=%s',
                53.25209, 34.37167 // Брянск
            ),
            false,
            stream_context_create(['http' => [
                'header' => (
                    'X-Yandex-API-Key: ' . config('yandex.api_weather_key')
                ),
            ]])
        );

        $temp = null;

        if ($result) {
            $result = json_decode($result);
            $temp   = $result->fact->temp;
        }

        return view('weather', ['temp' => $temp]);
    }
}
