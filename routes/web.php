<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route
    ::get('/', function () {return view('index');})
    ->name('index');

Route
    ::get('weather', 'WeatherController@getIndex')
    ->name('weather');

Route
    ::get('orders', 'OrdersController@getIndex')
    ->name('orders');
Route
    ::get('order/{id}', 'OrdersController@getOrder')
    ->where('id', '\d+')
    ->name('order');
Route
    ::post('order/{id}', 'OrdersController@updateOrder')
    ->where('id', '\d+')
    ->name('order-update');
